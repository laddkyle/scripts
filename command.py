#
# Command - For capturing command line output
# 
# command.py
#

import subprocess
import shlex

class Command(object):
    def __init__(self, cmd):
        self.mCmd = cmd
        self.mProc = subprocess.Popen(shlex.split(cmd), stdout=subprocess.PIPE)

    def pipe(self, cmd):
        """
        Pipe current proc to cmd
        """
        proc = subprocess.Popen(shlex.split(cmd), stdin=self.mProc.stdout,
                                 stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.mProc.stdout.close()
        self.mProc = proc
        return self

    def grep(self, pattern):
        """
        Shortcut for piping to grep
        """
        return self.pipe('grep ' + pattern)

    def sed(self, pattern):
        """
        Shortcut for piping to sed
        """
        return self.pipe('sed ' + pattern)

    def get(self):
        """
        Get the output of this command
        """
        return self.mProc.communicate()
