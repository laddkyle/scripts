# Scripts

### Command

Capture command line output in a convenient way in python

Ex: 

    user = Command("whoami").get()
    ps   = Command("ps aux").grep(user).get()

